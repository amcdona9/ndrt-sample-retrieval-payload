# First pass at UAV test code for working with the USB receiver
from pymavlink import mavutil
from argparse import ArgumentParser

# Format this for appropriate values on the Ras Pi
connection = mavutil.mavlink_connection('tcp:/dev/ttyUSB#:port#')

# Wait for the first heartbeat
# This sets the system and component ID of remote system for the link
connection.wait_heartbeat()
print("Heartbeat from system (system %u component %u)" % (connection.target_system, connection.target_system))
