# Testing script designed to be used with the "RFM9x_tx_Serial" file
# Important testing / debugging / usage note: creating a new serial connection restarts the Arduino's code!
# Useful for maintaining ground station operations
import serial
import threading

def commlink(device, cntLock, exitLock):
    global msgCnt
    global do_exit
    comms_up = False
    exit_sent = False
    while True:
        line = device.readline().decode('ascii').rstrip()
        if line != "":
            print("Received message from device: '{}'".format(line))
            with cntLock:
                msgCnt = msgCnt + 1
        # Let the Arduino initiate communications
        if line == "init":
            comms_up = True
        if comms_up:
            if "ERR: " in line:
                device.write(b'override')
            if line == "F.":
                print("Received shutdown confirmation from Arduino.")
                break
        if not exit_sent:
            with exitLock:
                if do_exit:
                    device.write(b'exit')
                    exit_sent = True

def user_handler(cntLock, exitLock):
    global msgCnt
    global do_exit
    while True:
        inp = input("Shut down the program? ")
        if inp[0] == 'Y':
            with cntLock:
                if msgCnt > 25:
                    with exitLock:
                        do_exit = True
                        print("Sending exit command to Arduino")
                        break
                else:
                    print("Cannot quit yet, it's too soon!")
        else:
            print("Ok, I'll wait then.")

msgCnt = 0
do_exit = False

if __name__ == "__main__":
    # Connect to the Arduino
    # This is the serial port on the right side of your computer.
    # Left side has a different port number; Ras Pi uses a different format altogether
    arduino = serial.Serial('/dev/cu.usbmodem14201', 9600, timeout=1)
    print(arduino.name)

    # Lock for the message counter; mostly just a proof-of-concept
    cntLock = threading.Lock()
    exitLock = threading.Lock()

    # Offload the actual communication to a thread (useful since the serial read blocks)
    # A second thread would / should handle user input
    t1 = threading.Thread(target=commlink, args=(arduino, cntLock, exitLock))
    t2 = threading.Thread(target=user_handler, args=(cntLock, exitLock))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    print("Done running")
