import random, math
# Simplifying assumptions for the simulation:
# - Field is 1 mile square, so 5280 x 5280 ft
# - The camera on the drone is mounted pointing straight down (may be updated later)
# - The camera's FOV angle defines a circular, rather than rectangular, region
# - The FEA sites are circular rather than square, with the equivalent area of 100 ft^2
# - Target detection is based on arc length
#   - Actual maximum resolution needed is 1.43 degrees; realistic max is 5.7 degrees; simulate a range of 3-30 for your threshold
# - Position vectors are [x, y, z], with y being the vertical axis
# - Air ceiling is 400 feet, so most algorithms operate at a fixed height of 200


PI = 3.14159265358

# Utility functions for treating lists as points
def ListDist(l1, l2):
    if len(l1) != len(l2):
        return None
    sum = 0
    for i in range(len(l1)):
        sum += math.pow(l1[i] - l2[i], 2)
    return math.sqrt(sum)

def ListAdd(l1, l2):
    if len(l1) != len(l2):
        return None
    l3 = list()
    for i in range(len(l1)):
        l3.append(l1[i] + l2[i])
    return l3

def ListDiff(l1, l2):
    if len(l1) != len(l2):
        return None
    l3 = list()
    for i in range(len(l1)):
        l3.append(l1[i] - l2[i])
    return l3

def ListMult(l1, c):
    l3 = list()
    for i in range(len(l1)):
        l3.append(l1[i] * c)
    return l3

def ListDiv(l1, c):
    l3 = list()
    for i in range(len(l1)):
        l3.append(l1[i] / c)
    return l3

def ListNorm(l1):
    sum = 0
    for item in l1:
        sum += item*item
    return math.sqrt(sum)

def ListUnit(l1):
    temp = ListNorm(l1)
    return ListDiv(l1, temp)

class Data():
    def __init__(self):
        self.atCenter = False
        self.hTarget = 40
        self.moe = 15
        self.tS = 0
        self.sweeping = False
        self.directionX = 1
        self.directionZ = 1
        self.lastZ = 0
        self.lastTheta = 0
        self.edge = 0

class Field():
    def __init__(self, spread = False):
        self.width = 5280
        self.height = 5280
        self.nTargets = 5
        self.targetR = 5.642
        self.targets = []
        self.minDist = 900
        for i in range(self.nTargets):
            tX = 0
            tZ = 0
            if spread:
                tX = random.randint(0, self.width)
                tZ = random.randint(0, self.height)
                while self.too_close(tX, tZ):
                    tX = random.randint(0, self.width)
                    tZ = random.randint(0, self.height)
            else:
                tX = random.randint(0, self.width)
                tZ = random.randint(0, self.height)
            self.targets.append((tX, 0, tZ))
    def Display(self):
        for coord in self.targets:
            print(coord)
    def too_close(self, x, z):
        for coord in self.targets:
            if ListDist(coord, (x, 0, z)) < self.minDist:
                return True
        return False


class Drone():
    def __init__(self, field, nav, data):
        self.field = field
        self.Control = nav
        self.Data = data
        posX = random.randint(0, field.width)
        posY = 0
        posZ = random.randint(0, field.height)
        self.speed = 5
        self.position = [posX, posY, posZ]
        self.found = False
        self.targetIDd = None
        self.FOV = PI/3
        self.viewThreshold = PI/36      # 5 degrees, minimum arc length needed to see target
        self.Time = 0
    def Scan(self):
        r = math.tan(self.FOV/2) * self.position[1]
        for target in self.field.targets:
            x2 = math.pow(self.position[0] - target[0], 2)
            z2 = math.pow(self.position[2] - target[2], 2)
            hDist = math.sqrt(x2 + z2) - self.field.targetR
            if hDist <= r:
                # If this is the case, the target is in the camera's view range and its apparent arc length needs to be determined
                theta0 = math.atan(hDist / self.position[1])
                theta1 = math.atan(min(r, hDist+2*self.field.targetR) / self.position[1])
                arcLength = theta1 - theta0
                if arcLength >= self.viewThreshold:
                    self.found = True
                    self.targetIDd = target
                    return True
        return False
    def Tick(self, dT):
        self.Time += dT
        posUpdate = self.Control(self, self.field, self.Data) # Pick a direction
        posUpdate = ListMult(ListUnit(posUpdate), self.speed) # Normalize the distance traveled
        self.position = ListAdd(self.position, posUpdate)
        if self.position[1] < 0: # The "crash" conditional
            self.position[1] = 0
        # After motion, scan for targets
        self.Scan()


# Simplest scanning algorithm: fly straight up until you see something
def UpScan(drone, field, data):
    return [0, 1, 0]

# Spiral out from the center of the field
def OutSpiral(drone, field, data):
    center = [field.width/2, data.hTarget, field.height/2]
    if not data.atCenter:
        if ListDist(drone.position, center) < data.moe:
            data.atCenter = True
            data.tS = drone.Time
        else:
            return ListDiff(center, drone.position)
    dist = ListNorm(ListDiff(drone.position, center))
    vX = 1
    vR = 0.22 - 0.2*math.sqrt(math.sqrt(dist / 2640.))
    if vR < 0:
        vR = 0
    theta = math.atan2(drone.position[2] - center[2], drone.position[0] - center[0])
    dX = -math.sin(theta)*vX + math.cos(theta)*vR
    dZ = math.cos(theta)*vX + math.sin(theta)*vR
    return [dX, 0, dZ]

# Sweep back and forth horizontally until something is found
def LinearSweep(drone, field, data):
    if not data.atCenter:
        destX = 0
        destZ = 0
        if drone.position[0] > field.width / 2:
            destX = field.width
            data.directionX = -1
        if drone.position[2] > field.height / 2:
            destZ = field.height
            data.directionZ = -1
        destination = (destX, data.hTarget, destZ)
        if ListDist(drone.position, destination) < data.moe:
            data.atCenter = True
        else:
            return ListDiff(destination, drone.position)
    if data.sweeping:
        destEdge = 2640*(1 + data.directionX)
        if abs(destEdge - drone.position[0]) < 2*data.moe:
            data.sweeping = False
            data.lastZ = drone.position[2]
        return (data.directionX, 0, 0)
    else:
        if abs(drone.position[2] - data.lastZ) > 70:
            data.sweeping = True
            data.directionX = -1*data.directionX
        return(0, 0, data.directionZ)

# Move to the center, and then sweep through pie slices until a target is found
def PieSweep(drone, field, data):
    center = [field.width/2, data.hTarget, field.height/2]
    if not data.atCenter:
        if ListDist(drone.position, center) < data.moe:
            data.atCenter = True
            data.lastTheta += PI / 26
            if data.lastTheta > PI:
                data.lastTheta -= 2*PI
            if data.lastTheta < PI/4 and data.lastTheta >= -1*PI/4:
                data.edge = 0
                data.directionZ = 1
            elif data.lastTheta < 3*PI/4 and data.lastTheta >= PI/4:
                data.edge = 1
                data.directionX = -1
            elif data.lastTheta < -3*PI/4 or data.lastTheta >= 3*PI/4:
                data.edge = 2
                data.directionZ = -1
            else:
                data.edge = 3
                data.directionX = 1
        return ListDiff(center, drone.position)
    if data.sweeping:
        if data.edge == 0 or data.edge == 2:
            if abs(drone.position[2] - data.lastZ) > 250:
                data.sweeping = False
                data.atCenter = False
            return (0, 0, data.directionZ)
        elif data.edge == 1 or data.edge == 3:
            if abs(drone.position[0] - data.lastZ) > 250:
                data.sweeping = False
                data.atCenter = False
            return (data.directionX, 0, 0)
    else:
        if data.edge == 0 and abs(drone.position[0] - field.width) < data.moe:
            data.sweeping = True
            data.lastZ = drone.position[2]
        elif data.edge == 1 and abs(drone.position[2] - field.height) < data.moe:
            data.sweeping = True
            data.lastX = drone.position[0]
        elif data.edge == 2 and abs(drone.position[0]) < data.moe:
            data.sweeping = True
            data.lastZ = drone.position[2]
        elif data.edge == 3 and abs(drone.position[2]) < data.moe:
            data.sweeping = True
            data.lastX = drone.position[0]
        return (math.cos(data.lastTheta), 0, math.sin(data.lastTheta))



def UnitTest(records, path):
    oob = 5280 # Out-of-bounds distance
    flags = Data()
    field = Field()
    uut = Drone(field, path, flags)
    while not uut.found and ListDist(uut.position, (field.width/2, 0, field.height/2)) < oob and uut.Time < 150000:
        uut.Tick(1)
        #print("{},{}".format(uut.position[0], uut.position[2]))
    '''if uut.found:
        print("Found target at {}".format(uut.targetIDd))
        print("After {} steps".format(uut.Time))
        print("At position {}".format(uut.position))
    else:
        print("Error: out of bounds.")
        print("Target not found after {} steps".format(uut.Time))
    return uut.Time'''
    records.append((uut.found, uut.Time))


if __name__ == "__main__":
    monteCarlo = []
    for i in range(5000):
        UnitTest(monteCarlo, OutSpiral)
        if i % 100 == 0:
            print("{} tests complete".format(i+1))
    for entry in monteCarlo:
        if entry[0]:
            print(entry[1])
