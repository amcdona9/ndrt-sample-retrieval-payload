import torch
import torch.nn as nn
import torch.utils.data
from torch.autograd import Variable
import numpy as np
from Autoencoder import Autoencoder


device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

#Hyperparameters
num_epochs = 10
batch_size = 5

#Load and transform the data
normalize = lambda x: (x.astype(np.double) - 128) / 128
images = np.load('images.npy')
images = normalize(images)
masks = np.load('masks.npy')
masks = 1*(1<masks[:,:,:,0])

images = torch.from_numpy(images).permute(0,3,1,2)
masks = torch.from_numpy(masks).unsqueeze(1).to(torch.float64)

dataset = torch.utils.data.TensorDataset(images,masks)
dataloader = torch.utils.data.DataLoader(dataset,batch_size=batch_size,shuffle=True)

model = Autoencoder().to(device).to(torch.float64)
distance = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(),weight_decay=1e-5)

loss_log = []

for epoch in range(num_epochs):
    for i,data in enumerate(dataloader):
        optimizer.zero_grad()
        img, target = data
        img = img.to(device)
        target = target.to(device)

        output = model(img)
        loss = distance(output,target)
        loss_log.append(loss)

        loss.backward()
        optimizer.step()

        if (i%20 == 0):
            print('epoch [{}/{}], loss:{:.4f}'.format(epoch+1,num_epochs,loss.data))
    torch.save(model.state_dict(),'weights.ckpt')
    np.save('loss.npy',np.array(loss_log))
