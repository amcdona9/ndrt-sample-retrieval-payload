import numpy as np
import cv2

cam = cv2.VideoCapture('crop3.mp4')

while True:
    retval, img = cam.read()
    
    res_scale = 0.5
    img = cv2.resize(img, (0, 0), fx=res_scale, fy=res_scale)
    
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lower = np.array([25, 49, 221])  # modified because it is bright outside
    upper = np.array([31, 148, 255])
    objmask = cv2.inRange(hsv, lower, upper)

    cv2.imshow("Binary image", objmask)
    
    kernel = np.ones((5, 5), np.uint8)
    objmask = cv2.morphologyEx(objmask, cv2.MORPH_CLOSE, kernel=kernel)
    
    cv2.imshow("Image after morphological operations", objmask)
    
    img, contours, h = cv2.findContours(objmask,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    img = np.zeros_like(img)
    for cnt in contours:
            approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
            #print(len(approx))

            if len(approx)==5:
                #print("pentagon")
                cv2.drawContours(img,[cnt],0,255,-1)
            elif len(approx)==3:
                #print("triangle")
                cv2.drawContours(img,[cnt],0,(0,255,0),-1)
            elif len(approx)==4:
                #print("square")
                cv2.drawContours(img,[cnt],0,(0,0,255),-1)
            elif len(approx) == 9:
                #print("half-circle")
                cv2.drawContours(img,[cnt],0,(255,255,0),-1)
            elif len(approx) > 15:
                #print("circle")
                cv2.drawContours(img,[cnt],0,(0,255,255),-1)
                
    cv2.imshow('Image after contour cropping',objmask)    
    action = cv2.waitKey(1)
                