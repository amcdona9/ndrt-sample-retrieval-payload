# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 17:12:20 2019

@author: Patrick
"""
import numpy as np

def find_cm(image):
    #image is an mxn binary mask
    x = [0,0]
    y = [0,0]
    
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i,j] == 1:
                x[0] += i
                y[0] += j
                x[1] += 1
                y[1] += 1
    cm = [int(x[0]/x[1]),int(y[0]/y[1])]
    return(cm)
    
    
"""
Find best color spectrum
Use actual statistics to make color boundary
Try Gabor Filter for texture, same as above
Figure out geometry
"""
    