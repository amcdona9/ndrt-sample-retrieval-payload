import cv2, glob
import numpy as np

def DisplayImage(image):
    #shows an image with opencv
    cv2.namedWindow('image')
    while True:
        cv2.imshow('image',image)
        key = cv2.waitKey(1) & 0xFF
    
        if key == ord('c'):
            break

#Load Images
img_path = glob.glob("./Photos/*")
images = [cv2.imread(i) for i in img_path]

#Color Space Transformation
trans_images = [cv2.cvtColor(i,cv2.COLOR_BGR2HLS) for i in images]

#Color Threshold
lower = np.array([10, 100, 140])
upper = np.array([60, 240, 255])
objmasks = [cv2.inRange(i, lower, upper) for i in trans_images]


#Morphological Operations
kernel = np.ones((5,5), np.uint8)
morphs = [cv2.morphologyEx(i, cv2.MORPH_DILATE, kernel=kernel) for i in objmasks]
morphs = [cv2.morphologyEx(i, cv2.MORPH_CLOSE, kernel=kernel) for i in morphs]
morphs = [cv2.morphologyEx(i, cv2.MORPH_ERODE, kernel=kernel) for i in morphs]
cv2.imwrite('test.jpg',morphs[0])

#Geometric Analysis
