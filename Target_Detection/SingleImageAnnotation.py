import cv2
import numpy as np
#import argparse for command line

#cam = cv2.VideoCapture('video3.mp4')

#frame = 1
#cropping = False

refPt = []

def click_and_crop(event, x, y, flags, param):
    global refPt
    
    if event == cv2.EVENT_LBUTTONUP:
        refPt.append([x,y])
        
        cv2.circle(image, (refPt[-1][0], refPt[-1][1]), 2, (0,255,0), 2)
        cv2.imshow("image", image)
        if len(refPt) >= 4:
            pts = np.array(refPt, np.int32)
            cv2.fillPoly(image, pts.reshape((1,-1,1,2)), (255,255,255))
            cv2.imshow('image', image)

image = cv2.imread('target.png')

clone = image.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', click_and_crop)

while True:
    cv2.imshow('image', image)    
    key = cv2.waitKey(1) & 0xFF
    
    #if r is pressed, reset cropping region
    if key == ord('r'):
        image = clone.copy()
        refPt = []
    elif key == ord('c'):
        break
    
def roi(img, vertices):
    mask = np.zeros_like(img)
    cv2.fillPoly(mask, vertices, 255)
    masked = cv2.bitwise_and(img, mask)
    return masked

pts = np.array(refPt, np.int32)
result = roi(clone.copy(),pts.reshape(1,-1,1,2))
    
cv2.destroyAllWindows()
    
    
    
    
    
    
    
    
    