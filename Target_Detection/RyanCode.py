import cv2

import numpy as np

cam = cv2.VideoCapture('crop3.mp4')

while (True):

    retval, img = cam.read()

    res_scale = 0.5  # rescale the input image if it's too large

    img = cv2.resize(img, (0, 0), fx=res_scale, fy=res_scale)

    # detect selected color (OpenCV uses BGR instead of RGB)

    # this example is tuned to blue, in a relatively dark room

    # lower = np.array([50, 0, 0])

    # upper = np.array([100, 50, 50])

    # objmask = cv2.inRange(img, lower, upper)

    # # uncomment this if you want to use HSV

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lower = np.array([10, 50, 200])  # modified because it is bright outside
    upper = np.array([40, 150, 255])
    objmask = cv2.inRange(hsv, lower, upper)

    # you may use this for debugging

    cv2.imshow("Binary image", objmask)

    # Resulting binary image may have large number of small objects.

    # You may check different morphological operations to remove these unnecessary

    # elements. You may need to check your ROI defined in step 1 to

    # determine how many pixels your candy may have.

    kernel = np.ones((5, 5), np.uint8)

    objmask = cv2.morphologyEx(objmask, cv2.MORPH_CLOSE, kernel=kernel)

    objmask = cv2.morphologyEx(objmask, cv2.MORPH_DILATE, kernel=kernel)

    cv2.imshow("Image after morphological operations", objmask)

    # find connected components

    cc = cv2.connectedComponents(objmask)

    ccimg = cc[1].astype(np.uint8)
    # find contours of these objects

    imc, contours, hierarchy = cv2.findContours(ccimg,

                                                cv2.RETR_TREE,

                                                cv2.CHAIN_APPROX_SIMPLE)

    # You may display the countour points if you want:

    #cv2.drawContours(img, contours, -1, (0,255,0), 3)

    # ignore bounding boxes smaller than "minObjectSize"
    minObjectSize = 20

    for cont in contours:

        # test shape to see if it is a square
        peri = cv2.arcLength(cont, True)

        approx = cv2.approxPolyDP(cont, 0.04 * peri, True)

        if len(approx) == 4:
            # compute the bounding box of the contour and use the
            # bounding box to compute the aspect ratio
            (x, y, w, h) = cv2.boundingRect(approx)
            ar = w / float(h)

            # a square will have an aspect ratio that is approximately
            # equal to one, otherwise, the shape is a rectangle
            square = 1 if ar >= 0.5 and ar <= 1.5 else 0  # prove it loosely resembles a square
        else:
            square = 0

        if square:
            new_objmask = np.zeros(objmask.shape)
            new_objmask[x:x+w,y:y+h] = objmask[x:x+w,y:y+h]

            # use just the first contour to draw a rectangle

            x, y, w, h = cv2.boundingRect(cont)

            # do not show very small objects

            if (w > minObjectSize or h > minObjectSize) and square:
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 3)

                cv2.putText(img,  # image

                            "FEA Detected",  # text

                            (x, y - 10),  # start position

                            cv2.FONT_HERSHEY_SIMPLEX,  # font

                            0.7,  # size

                            (0, 255, 0),  # BGR color

                            1,  # thickness

                            cv2.LINE_AA)  # type of line

        cv2.imshow("Live WebCam", img)
        cv2.imshow("New Image", new_objmask)
        action = cv2.waitKey(1)
    
    if action == 27:
        break