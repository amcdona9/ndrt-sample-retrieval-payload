# Computer Vision Course (CSE 40535/60535)
# University of Notre Dame, Fall 2018
# ________________________________________________________________
# Adam Czajka, Andrey Kuehlkamp, September 2017

# Here are your tasks for today:
#
# Task 1 (1 points):
# - Select one candy that you want to track and set the RGB
#   channels to the selected ranges (found by hsvSelection.py).
# - Check if HSV color space works better. Can you ignore one or two
#   channels when working in HSV color space? Why?
# - Try to track candies of different colors (blue, yellow, green).
# - Upload your best solution for one candy/color to your Dropbox as colorTracking1.py
#
# Task 2 (1 point):
# - Adapt your code to track multiple candies of *the same* color simultaneously.
# - Upload your solution to your Dropbox as colorTracking2.py
#
# Task 3 (2 points):
# - Adapt your code to track multiple candies of *different* colors simultaneously.
# - Upload your solution to your Dropbox as colorTracking3.py
#
# Present your solutions to the instructor / TA in class.
# Or upload them to your Dropbox by Monday, Sept. 10, 11:59 pm.

"""
test different color spaces L*a*b, HSL, look at opencv
different channels separately
process single channel
play with color spaces, channels, see which is most clear

might be good to fly on site to calibrate flight value

grid search
hyperparameter tuning
intersection over union
ground truth anotations
overlap/combined area
hemming distance-M1nM2
annotate corners of tarp
click corners
set inside to 1, outside to 0
create map

collect distribution of H for all videos
figure out where to bound distribution-95%-set as lower/upper bound

annotate every 10th/20th frame

segnet(segment?)-possible convnet
don't train from scratch
look for good, light segmentation network
movistick-usb extension for deep learning, raspberry pi
restricted to certain architectures
prone to failure on site

texture: edge detection, wavelets
low numbers on target, high numbers outside target
can remove false positive
geometry: can measure geometry of detected region
perimeter vs diameter

potentially use support vector machine

feature: diameter/perimeter
area
major/minor axis ratio

several features, have to decide if target or not
use svm to classify given feature vector

first step: annotating targets
"""



import cv2

import numpy as np



cam = cv2.VideoCapture('video3.mp4')



while (True):

    retval, img = cam.read()



    res_scale = 0.5             # rescale the input image if it's too large

    img = cv2.resize(img, (0,0), fx = res_scale, fy = res_scale)



    # detect selected color (OpenCV uses BGR instead of RGB)

    # this example is tuned to blue, in a relatively dark room

    # lower = np.array([50, 0, 0])

    # upper = np.array([100, 50, 50])

    # objmask = cv2.inRange(img, lower, upper)



    # # uncomment this if you want to use HSV

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)

    lower = np.array([25, 159, 158])

    upper = np.array([31, 230, 255])

    objmask = cv2.inRange(hsv, lower, upper) #probably what generates shading



    # you may use this for debugging

    cv2.imshow("Binary image", objmask)



    # Resulting binary image may have large number of small objects.

    # You may check different morphological operations to remove these unnecessary

    # elements. You may need to check your ROI defined in step 1 to

    # determine how many pixels your candy may have.

    kernel = np.ones((5,5), np.uint8)
    #objmask = cv2.morphologyEx(objmask, cv2.MORPH_OPEN, kernel=kernel)
    objmask = cv2.morphologyEx(objmask, cv2.MORPH_DILATE, kernel=kernel)
    objmask = cv2.morphologyEx(objmask, cv2.MORPH_CLOSE, kernel=kernel)
    objmask = cv2.morphologyEx(objmask, cv2.MORPH_ERODE, kernel=kernel)
    
    #cv2.imshow('objmask1', objmask)

    cv2.imshow("Image after morphological operations", objmask)



    # find connected components

    cc = cv2.connectedComponents(objmask)

    ccimg = cc[1].astype(np.uint8)



    # find contours of these objects

    imc, contours, hierarchy = cv2.findContours(ccimg,

                                                cv2.RETR_TREE,

                                                cv2.CHAIN_APPROX_SIMPLE)



    # You may display the countour points if you want:

    # cv2.drawContours(img, contours, -1, (0,255,0), 3)


    # ignore bounding boxes smaller than "minObjectSize"
    minObjectSize = 20;
    

    for cont in contours:

        # use just the first contour to draw a rectangle

        x, y, w, h = cv2.boundingRect(cont)

        # do not show very small objects

        if w > minObjectSize or h > minObjectSize:

            cv2.rectangle(img, (x, y), (x+w, y+h), (0,255,0), 3)

            cv2.putText(img,                        # image

                        "Here's my candy!",         # text

                        (x, y-10),                  # start position

                        cv2.FONT_HERSHEY_SIMPLEX,   # font

                        0.7,                        # size

                        (0, 255, 0),                # BGR color

                        1,                          # thickness

                        cv2.LINE_AA)                # type of line



    cv2.imshow("Live WebCam", img)



    action = cv2.waitKey(1)

    if action==27:

        break

