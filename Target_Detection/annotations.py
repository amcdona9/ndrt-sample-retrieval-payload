import cv2
import numpy as np

cam = cv2.VideoCapture('crop3.mp4')

frame = 0

imgset = []
maskset = []

def click_and_crop(event, x, y, flags, param):
    global refPt
    
    if event == cv2.EVENT_LBUTTONUP:
        refPt.append([x,y])
        
        cv2.circle(image, (refPt[-1][0], refPt[-1][1]), 2, (0,255,0), 2)
        cv2.imshow("image", image)
        if len(refPt) >= 4:
            pts = np.array(refPt, np.int32)
            cv2.fillPoly(image, pts.reshape((1,-1,1,2)), (255,255,255))
            cv2.imshow('image', image)

def roi(img, vertices):
    mask = np.zeros_like(img)
    cv2.fillPoly(mask, vertices, 255)
    masked = cv2.bitwise_and(img, mask)
    return masked

print("""Controls: click on boundaries to create polygon
      r-reset annotations
      n-skip frame
      c-finished with frame
      q-quit annotating
      s-skip ahead
""")

while True:
    stay1 = True
    stay2 = True
    if frame % 100 == 0:  
        
        retval, image = cam.read()
        refPt = []

        clone = image.copy()
        cv2.namedWindow('image')
        cv2.setMouseCallback('image', click_and_crop)

        while True:
            cv2.imshow('image', image)    
            key = cv2.waitKey(1) & 0xFF
    
    #if r is pressed, reset cropping region
            if key == ord('r'):
                image = clone.copy()
                refPt = []
            elif key == ord('n'):
                stay1 = False
                break
            elif key == ord('c'):
                break
            elif key == ord('q'):
                np.save('images.npy',np.array(imgset))
                np.save('masks.npy', np.array(maskset))
                stay2 = False
                break
            elif key == ord('s'):
                frame += 1000
                stay1 = False
                break
        
        if stay1:
            pts = np.array(refPt, np.int32)
            result = roi(clone.copy(),pts.reshape(1,-1,1,2))
            maskset.append(result)
            imgset.append(clone)
    frame += 1
    if frame % 100000 == 0:
        np.save('images.npy',np.array(imgset))
        np.save('masks.npy', np.array(maskset))
    
    if not stay2:
         
        cv2.destroyAllWindows()
        break



    
    
    
    
    
    
    
    