import numpy as np
import cv2
#import matplotlib.pyplot as plt
import itertools

def DisplayImage(image):
    #shows an image with opencv
    cv2.namedWindow('image')
    while True:
        cv2.imshow('image',image)
        key = cv2.waitKey(1) & 0xFF
    
        if key == ord('c'):
            break

def IntersectOverUnion(region, target):
    #region is binary image identified by computer, target is reference
    intersect = np.sum(region*target)
    union = np.sum(region+target-region*target)
    return(intersect/union)
    
def AlternateError(region, target):
    invtarget = target*-1+1 #inverts the target
    intersect = np.sum(region*invtarget)
    return(intersect) #returns the number of identified pixels outside the boundary


def ColorBounds(images, masks, percentinclude=95):
    #takes in images and target masks as input, gathers all spectrum values,
    #and returns all values within target percentile 
    bounds = []
    data = []
    if images.ndim == 3:
        channels = 1
    else:
        channels = images.shape[3]
    
    for channelnum in range(channels):
        if channels == 1:
            values = images*masks
        else:    
            values = images[:,:,:,channelnum] * masks
        
        values = values.reshape([-1])
        
        values = values[np.nonzero(values)]
        data.append(values)
        
        upper = np.mean([percentinclude,100])
        Q3 = np.percentile(values,upper)
        Q1 = np.percentile(values,100-upper)
        
        bounds.append([Q1,Q3])
        #print((Q1,Q3))

        #print(np.sum(-1*(Q1>values) + 1*(Q3>= values))/len(values))
     
    return(bounds,data)

def TestColorSpace(images, colorspace=None, omitchannels=[], percentinclude=95, addons = []):
    try:
        transformedimg = np.array([cv2.cvtColor(images[i],colorspace) for i in range(images.shape[0])])
    except:
        transformedimg = images
    
    if len(addons) > 0:
        addons = np.reshape(addons,(addons.shape[0],addons.shape[1],addons.shape[2],1))
        transformedimg = np.concatenate((transformedimg,addons),axis=3)

    
    
    bounds, data = ColorBounds(transformedimg,masks, percentinclude)
    bounds = np.array(bounds).T
    
    for channel in omitchannels:
        bounds[0,channel] = 0
        bounds[1,channel] = 255
    #print(bounds)
    objmasks = np.array([cv2.inRange(image,bounds[0],bounds[1]) for image in transformedimg])/255
    
    return(objmasks, bounds)
    
def ApplyMorphology(images,*argv):
    """
    applies each morphology passed in order.
    erosion = cv2.erode(img,kernel,iterations = 1)
    dilation = cv2.dilate(img,kernel,iterations = 1)
    opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
    gradient = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)
    tophat = cv2.morphologyEx(img, cv2.MORPH_TOPHAT, kernel)
    blackhat = cv2.morphologyEx(img, cv2.MORPH_BLACKHAT, kernel)
    
    Usage: throw in each morphology wanted in the sequence wanted
    """
    basicmorphologies = {'erosion':cv2.erode,'dilation':cv2.dilate}
    advancedmorphologies = {'opening':cv2.MORPH_OPEN,'closing':cv2.MORPH_CLOSE,
                            'gradient':cv2.MORPH_GRADIENT,
                            'tophat':cv2.MORPH_TOPHAT,
                            'blackhat':cv2.MORPH_BLACKHAT}
    kernel = np.ones((5,5),np.uint8)
    
    for arg in argv:
        if arg in basicmorphologies.keys():
            images = [basicmorphologies[arg](image,kernel,iterations=1) for image in images]
        elif arg in advancedmorphologies.keys():
            images = [cv2.morphologyEx(image,advancedmorphologies[arg],kernel) for image in images]
    
    return(images)
    
def TextureFiltering(images,masks,ksize=(5,5),sigma=1,theta=1,lambd=1,gamma=1,return_mask=True):
    gabor_kernel = cv2.getGaborKernel(ksize,sigma,theta,lambd,gamma)
    output_images = np.array([np.sum(cv2.filter2D(image,-1,gabor_kernel),axis=2) for image in images],dtype=np.uint8)
    if not return_mask:
        return(output_images)
        
    objmasks = TestColorSpace(output_images)
    
    return(output_images,objmasks)
    

def TestMasks(masks,objmasks):
    return([IntersectOverUnion(objmasks[i],masks[i]) for i in range(masks.shape[0])])

def AlternateTest(masks,objmasks):
    return([AlternateError(objmasks[i],masks[i]) for i in range(masks.shape[0])])

def TestContours(masks):
    masks = masks.astype(np.uint8)
    images = []
    approxes = []
    
    for mask in masks:
        img, contours, h = cv2.findContours(mask,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
        img = np.zeros_like(img)
        
        current_images = []
        sums_list = []
        
        for cnt in contours:
            approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
            #print(len(approx))
            approxes.append(len(approx))
            if len(approx)==5:
                #print("pentagon")
                cv2.drawContours(img,[cnt],0,255,-1)
            elif len(approx)==3:
                #print("triangle")
                cv2.drawContours(img,[cnt],0,(0,255,0),-1)
            elif len(approx)==4:
                #print("square")
                cv2.drawContours(img,[cnt],0,(0,0,255),-1)
            elif len(approx) == 9:
                #print("half-circle")
                cv2.drawContours(img,[cnt],0,(255,255,0),-1)
            elif len(approx) > 15:
                #print("circle")
                cv2.drawContours(img,[cnt],0,(0,255,255),-1)
            else:
                cv2.drawContours(img,[cnt],0,255,-1)


            current_images.append(img/255)
        
        if len(current_images) == 1:
            images.append(current_images[0])
        else:
            sums = [np.sum(i) for i in current_images]
            sums_list.append(sums)

            images.append(current_images[sums.index(np.max(sums))])
    
    return(images,approxes)




images = np.load('images.npy')
masks = np.load('masks.npy')
masks = 1*(1<masks[:,:,:,0])


objmasks, bounds = TestColorSpace(images, cv2.COLOR_BGR2HLS)
morphmasks = np.array(ApplyMorphology(objmasks,'dilation','closing','erosion'))
print(np.mean(TestMasks(masks, objmasks)))
print(np.mean(TestMasks(masks, morphmasks)))
"""
objmasks, bounds = TestColorSpace(images, cv2.COLOR_BGR2HSV)
morphmasks = np.array(ApplyMorphology(objmasks, 'closing'))
"""
#none=BGR
"""
colors = [None,cv2.COLOR_BGR2GRAY,cv2.COLOR_BGR2BGRA,cv2.COLOR_BGR2BGR565,
          cv2.COLOR_BGR2BGR555,cv2.COLOR_BGR2XYZ,cv2.COLOR_BGR2YCrCb,
          cv2.COLOR_BGR2HSV,cv2.COLOR_BGR2Lab, cv2.COLOR_BGR2Luv,cv2.COLOR_BGR2HLS,
          cv2.COLOR_BGR2YUV]

morphs = ['erosion','dilation','opening','closing','gradient','tophat','blackhat']

morph_combinations_1 = morphs
morph_combinations_2 = [i for i in itertools.product(morphs,repeat=2)]
morph_combinations_3 = [i for i in itertools.product(morphs,repeat=3)]

image = images[0]
for color in colors[1:]:
    img = cv2.cvtColor(image,color)

"""
"""
results_1 = np.zeros((len(colors),len(morph_combinations_1),4))
results_2 = np.zeros((len(colors),len(morph_combinations_2),4))
results_3 = np.zeros((len(colors),len(morph_combinations_3),4))
for i in range(len(colors)):
    for j in range(len(morph_combinations_1)):
        print((i,j))
        objmasks,bounds = TestColorSpace(images,colors[i])
        morphmasks = np.array(ApplyMorphology(objmasks,morph_combinations_1[j]))
        
        results_2[i,j,0] = np.mean(TestMasks(masks,objmasks))
        results_2[i,j,1] = np.mean(TestMasks(masks,morphmasks))
        results_2[i,j,2] = np.mean(AlternateTest(masks,objmasks))
        results_2[i,j,3] = np.mean(AlternateTest(masks,morphmasks))
        
    for j in range(len(morph_combinations_2)):
        print((i,j))
        objmasks,bounds = TestColorSpace(images,colors[i])
        morphmasks = np.array(ApplyMorphology(objmasks,morph_combinations_2[j][0],morph_combinations_2[j][1]))
        
        results_2[i,j,0] = np.mean(TestMasks(masks,objmasks))
        results_2[i,j,1] = np.mean(TestMasks(masks,morphmasks))
        results_2[i,j,2] = np.mean(AlternateTest(masks,objmasks))
        results_2[i,j,3] = np.mean(AlternateTest(masks,morphmasks))
        
    for j in range(len(morph_combinations_3)):
        print((i,j))
        objmasks,bounds = TestColorSpace(images,colors[i])
        morphmasks = np.array(ApplyMorphology(objmasks,morph_combinations_3[j][0],morph_combinations_3[j][1],morph_combinations_3[j][2]))
        
        results_3[i,j,0] = np.mean(TestMasks(masks,objmasks))
        results_3[i,j,1] = np.mean(TestMasks(masks,morphmasks))
        results_3[i,j,2] = np.mean(AlternateTest(masks,objmasks))
        results_3[i,j,3] = np.mean(AlternateTest(masks,morphmasks))
        
        
        
np.save('results_1.npy',results_1)
np.save('results_2.npy',results_2)
np.save('results_3.npy',results_3)
"""


"""
contmasks,approxes = TestContours(morphmasks)
contmasks = np.array(contmasks)


print(np.mean(TestMasks(masks,objmasks)))
print(np.mean(TestMasks(masks,morphmasks)))
print(np.mean(TestMasks(masks,contmasks)))

print(np.mean(AlternateTest(masks,objmasks)))
print(np.mean(AlternateTest(masks,morphmasks)))
print(np.mean(AlternateTest(masks,contmasks)))
#shows that there are fewer false positives after geometry



distinguish between the contours





#textureimages = TextureFiltering(images,masks,return_mask=False)
#objmasks = TestColorSpace(images,cv2.COLOR_BGR2HSV,addons=textureimages)
#morphmasks = ApplyMorphology(objmasks,'closing')
#print(np.mean(TestMasks(masks,objmasks)))
#print(np.mean(TestMasks(masks,morphmasks)))

#objmasks = TestColorSpace(images, cv2.COLOR_BGR2HSV)
#morphmasks = ApplyMorphology(objmasks, 'closing')
#print(np.mean(TestMasks(masks,objmasks)))
#print(np.mean(TestMasks(masks,morphmasks)))
#textureimages,objmasks = TextureFiltering(images,masks)
#morphmasks = ApplyMorphology(objmasks, 'closing')
#print(np.mean(TestMasks(masks,morphmasks)))
#plt.imshow(morphmasks[0])

Workflow per spectrum/channel:
calculate bounds with Colorbounds
get algorithm's predictions
evaluate
-compare results
"""




